package example01;

public class CoverForIPhone {
    private iPhone iPhone;

    public CoverForIPhone(iPhone iPhone) {
        this.iPhone = iPhone;
    }

    public iPhone getPhone() {
        return iPhone;
    }
}
