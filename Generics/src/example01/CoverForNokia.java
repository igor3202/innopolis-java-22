package example01;

public class CoverForNokia {
    private Nokia nokia;

    public CoverForNokia(Nokia nokia) {
        this.nokia = nokia;
    }

    public Nokia getPhone() {
        return nokia;
    }
}
