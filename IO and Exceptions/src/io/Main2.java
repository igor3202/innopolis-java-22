package io;

import java.io.*;

public class Main2 {
    public static void main(String[] args) {
        try {
            Reader reader = new FileReader("input.txt");
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();
            System.out.println(line);
            line = bufferedReader.readLine();
            System.out.println(line);
            line = bufferedReader.readLine();
            System.out.println(line);
        } catch (IOException e) {
            System.out.println("Какие-то ошибки");
        }
    }
}
