package exceptions;

import java.util.Scanner;

public class MainArithmeticException {

    public static int div(int x, int y) throws ArithmeticException {
        return x / y;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            int a = scanner.nextInt();
            int b = scanner.nextInt();
            try {
                int c = div(a , b);
                System.out.println(c);
            } catch (ArithmeticException e) {
                System.out.println("Вы наверно поделили на ноль, давайте еще раз");
            }
        }

    }
}
