package realworld;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        UsersRepository usersRepository = new UsersRepositoryFileBasedImpl("input.txt");
        System.out.println(usersRepository.findFirstWithMinAge());


    }
}
