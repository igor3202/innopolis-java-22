public interface Set<V> {
    void put(V v);

    boolean contains(V v);
}
