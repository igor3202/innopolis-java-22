import java.util.Objects;

public class Human {
    private final String name;
    private final int age;
    private final double height;

    public Human(String name, int age, double height) {
        this.name = name;
        this.age = age;
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public double getHeight() {
        return height;
    }

//    @Override
//    public int hashCode() {
//        int ageHashCode = age;
//        int nameHashCode = name.hashCode();
//        int heightHashCode = Double.hashCode(height);
//        return ageHashCode + 31 * nameHashCode + 31 * 31 * heightHashCode;
//    }


    @Override
    public int hashCode() {
        int result;
        long temp;
        result = name != null ? name.hashCode() : 0;
        result = 31 * result + age;
        temp = Double.doubleToLongBits(height);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Human human = (Human) o;

        if (age != human.age) return false;
        if (Double.compare(human.height, height) != 0) return false;
        return Objects.equals(name, human.name);
    }
}
