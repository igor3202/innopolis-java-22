package ru.inno.education.repositories;

import ru.inno.education.models.Student;

import java.util.List;

public interface StudentsRepository {
    List<Student> findAll();

    void save(Student student);
}
