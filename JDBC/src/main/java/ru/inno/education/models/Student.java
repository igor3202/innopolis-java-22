package ru.inno.education.models;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Student {
    private Long id;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private Integer age;
    private Boolean isWorker;
    private Double average;
}
