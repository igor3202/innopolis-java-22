package ru.inno.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;

@ComponentScan(basePackages = "ru.inno")
@PropertySource(value = "classpath:application.properties")
@Configuration
public class AppConfig {

    @Autowired
    private Environment environment;

    @Bean
    public DataSource dataSource() {
        HikariDataSource hikariDataSource = new HikariDataSource();
        hikariDataSource.setUsername(environment.getProperty("db.username"));
        hikariDataSource.setPassword(environment.getProperty("db.password"));
        hikariDataSource.setMaximumPoolSize(environment.getProperty("db.hikari.maxPoolSize", Integer.class));
        hikariDataSource.setJdbcUrl(environment.getProperty("db.url"));

        return hikariDataSource;
    }
}
