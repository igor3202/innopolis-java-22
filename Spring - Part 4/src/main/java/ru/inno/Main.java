package ru.inno;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.inno.config.AppConfig;
import ru.inno.services.UsersService;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

        UsersService usersService = context.getBean(UsersService.class);

        usersService.signUp("marsel@marsel.com", "qwertyu199");
    }
}
