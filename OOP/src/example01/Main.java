package example01;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Human marsel = new Human(1.85, true);

        Human notMarsel = new Human(1.7, false);

        Scanner scanner = new Scanner(System.in);
        marsel.height = scanner.nextDouble();

        marsel.grow(0.05);
        marsel.relax();

        notMarsel.grow(0.3);
        notMarsel.work();

        System.out.println(marsel.height + " " + marsel.isWorker);
        System.out.println(notMarsel.height + " " + notMarsel.isWorker);
    }
}
