package example02;

public class User {

    public final static int MIN_ADULT_AGE = 18;
    public final static int MAX_ADULT_AGE = 90;
    public final static int DEFAULT_ADULT_AGE = MAX_ADULT_AGE;

    private String name;
    private int age;

    User(String name, int age) {
        this.name = name;

        if (age >= MIN_ADULT_AGE && age <= MAX_ADULT_AGE) {
            this.age = age;
        } else {
            this.age = DEFAULT_ADULT_AGE;
        }
    }

    // геттер - метод доступа
    int getAge() {
        return this.age;
    }
}
