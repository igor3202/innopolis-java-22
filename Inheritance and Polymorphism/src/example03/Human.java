package example03;

public class Human {
    protected String firstName;
    protected String lastName;
    private int age; // 0
    private boolean isSleep; // false
    private boolean isMarried; // false

    public Human(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public void setAge(int age) {
        if (age >= 0 && age <= 120) {
            this.age = age;
        } else {
            this.age = 0;
        }
    }

    public void setSleep(boolean sleep) {
        isSleep = sleep;
    }

    public void setMarried(boolean married) {
        isMarried = married;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public boolean isSleep() {
        return isSleep;
    }

    public boolean isMarried() {
        return isMarried;
    }

    public void go() {
        System.out.println("Я человек и я иду куда-то!!");
    }
}
