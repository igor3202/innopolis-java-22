package example03;

public class Main {

    // полиморфизм
    public static void goOnWork(Human[] humans) {
        for (int i = 0; i < humans.length; i++) {
            humans[i].go();
        }
    }

    public static void main(String[] args) {
//        Human h = new Human("Marsel", "Sidikov");
//        Programmer p = new Programmer("Victor", "Ivanov", "PHP");
//        FrontendDeveloper fd = new FrontendDeveloper();
//        Worker w = new Worker("Игорь", "Игорев");
        // Human a2 = p; Human a3 = fd; Human a4 = w;

        Human a1 = new Human("Marsel", "Sidikov");
        Programmer a2 = new Programmer("Victor", "Ivanov", "PHP");
        FrontendDeveloper a3 = new FrontendDeveloper();
        Worker a4 = new Worker("Игорь", "Игорев");

        int i = 0;

        Human[] workers = {a1, a2, a3, a4};

        goOnWork(workers);



    }
}
