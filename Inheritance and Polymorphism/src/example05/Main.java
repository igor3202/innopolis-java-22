package example05;

public class Main {

    // полиморфизм
    public static void goOnWork(Human[] humans) {
        for (int i = 0; i < humans.length; i++) {
            humans[i].go();
        }
    }

    public static void giveMoney(Professional[] professionals) {
        for (int i = 0; i < professionals.length; i++) {
            professionals[i].giveMoney(100);
            professionals[i].goVacation();
        }
    }

    public static void goFromWork(Runner[] runners) {
        for (int i = 0; i < runners.length; i++) {
            runners[i].runFromWork();
        }
    }

    public static void main(String[] args) {
        Programmer programmer = new Programmer("Victor", "Ivanov", "PHP");
        FrontendDeveloper frontendDeveloper = new FrontendDeveloper();
        Worker worker = new Worker("Игорь", "Игорев");
        Student student = new Student("Максим", "Поздеев", 3);

        Human[] humans = {programmer, frontendDeveloper, worker, student};
        Professional[] professionals = {programmer, frontendDeveloper, worker};
        Runner[] runners = {worker, student};

        goOnWork(humans);
        giveMoney(professionals);
        goFromWork(runners);

    }
}
