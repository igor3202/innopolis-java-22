package example05;

public class Student extends Human implements Runner {

    private double averageMark;

    public Student(String firstName, String lastName, double averageMark) {
        super(firstName, lastName);
        this.averageMark = averageMark;
    }

    public double getAverageMark() {
        return averageMark;
    }

    @Override
    public void go() {
        System.out.println("А я только учусь, никуда не хожу");
    }

    @Override
    public void tellAbout() {
        System.out.println("Я студент!");
    }

    @Override
    public void runFromWork() {
        System.out.println("Не увольняйте!!!");
    }
}
