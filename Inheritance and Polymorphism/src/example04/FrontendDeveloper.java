package example04;

public class FrontendDeveloper extends Programmer {

    public FrontendDeveloper() {
        super("hello", "bye", "CSS/HTML/JS");
    }

    @Override
    public void go() {
        System.out.println("Я никуда не иду, я просто сижу и верстаю страницы");
    }
}
