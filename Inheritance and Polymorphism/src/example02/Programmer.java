package example02;

public class Programmer extends Human {

    private int experience;
    private String programmingLanguage;

    public Programmer(String firstName, String lastName, String programmingLanguage) {
        super(firstName, lastName);
        this.programmingLanguage = programmingLanguage;
    }

    public void setExperience(int experience) {
        if (experience > 0 && experience <= 120) {
            this.experience = experience;
        }
    }

    public int getExperience() {
        return experience;
    }

    public String getProgrammingLanguage() {
        return programmingLanguage;
    }

    public void tellAbout() {
        System.out.println("Hello, my name is " + firstName + " My experience is = " + experience + ", my programming language =  " + programmingLanguage);
    }

    public void go() {
        System.out.println("Я программист и я иду домой");
    }
}
