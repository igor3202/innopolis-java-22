package example02;

public class Main {
    public static void main(String[] args) {
        Human human = new Human("Марсель", "Сидиков");
        human.setAge(-10);

        Programmer programmer =
                new Programmer("Виктор", "Евлампьев",
                        "PHP");

        FrontendDeveloper frontendDeveloper = new FrontendDeveloper();

        programmer.setAge(26);
        programmer.setExperience(13);
        programmer.setSleep(true);
        human.firstName = "Hello!";
        human.go();
        programmer.go();
        human.setSleep(false);

    }
}
