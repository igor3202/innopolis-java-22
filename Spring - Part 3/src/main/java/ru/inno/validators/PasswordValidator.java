package ru.inno.validators;

public interface PasswordValidator {

    /**
     * Проверяет корректность пароля
     * @param password пароль, которую нужно проверить
     * @throws IllegalArgumentException если пароль не является корректным
     */
    void validate(String password) throws IllegalArgumentException;
}
