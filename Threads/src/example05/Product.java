package example05;

public class Product {
    private boolean isReady;

    // isReady = true, isConsumed = false
    // isReady = false, isConsumed = true
    public boolean isConsumed() {
        return !isReady;
    }

    // isReady = true, isProduced = true
    // isReady = false, isProduced = false
    public boolean isProduced() {
        return isReady;
    }

    public void produce() {
        isReady = true;
    }

    public void consume() {
        isReady = false;
    }
}
