package example05;

public class Producer extends Thread {
    private final Product product;

    public Producer(Product product) {
        super("Producer");
        this.product = product;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (product) {
                System.out.println("Producer: Проверяет, не использован ли продукт продукта");
                while (!product.isConsumed()) {
                    System.out.println("Producer: Продукт еще не использован, уходим в ожидание");
                    try {
                        product.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }

                System.out.println("Producer: Подготавливаем продукт");
                product.produce();
                System.out.println("Producer: Оповещаем Consumer-a");
                product.notify();
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

    }
}

