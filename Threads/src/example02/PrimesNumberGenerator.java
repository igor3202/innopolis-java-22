package example02;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class PrimesNumberGenerator {

    public void generateNumbers(String fileName, int from, int to) {
        Thread newThread = new Thread(() -> {
            try (BufferedWriter writer = new BufferedWriter(
                    new FileWriter(fileName, true))) {
                System.out.println(Thread.currentThread().getName() + ": Начали запись в файл - " + fileName);
                for (int i = from; i <= to; i++) {
                    if (isPrime(i)) {

                        writer.write(i + "\n");

                    }
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
            System.out.println(Thread.currentThread().getName() + ": Закончили запись в файл - " + fileName);
        });

        newThread.start();
    }

    // на вход число, на выход - true, если простое, false - если не простое
    private boolean isPrime(int number) {
        if (number == 0 || number == 1) {
            throw new IllegalArgumentException("Число не является корректным");
        }

        if (number == 2 || number == 3) {
            return true;
        }

        // 121 -> 2, 3, 4, 5, 6, 7, 8, 9, 10, 11
        // 113 -> 2, 3, 4, 5, 6, 7, 8, 9, 10
        // i < sqrt(n) => i * i <= n
        for (int i = 2; i * i <= number; i++) {
            if (number % i == 0) {
                return false;
            }
        }

        return true;
    }
}
