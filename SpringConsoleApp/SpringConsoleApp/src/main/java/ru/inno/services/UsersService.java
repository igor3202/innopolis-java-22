package ru.inno.services;

import ru.inno.models.User;

import java.util.List;

public interface UsersService {
    List<User> getAllUsers();
}
