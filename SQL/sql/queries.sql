-- получение всех студентов
select *
from student;
-- получение только имени и фамилии студентов
select first_name, last_name
from student;
-- получение студентов, упорядоченных по возрасту (если возраст совпал - по id)
select *
from student
order by age, id;
-- получение студентов, упорядоченных по id в обратном порядке
select *
from student
order by id desc;
-- получить все уникальные возраста
select distinct(age)
from student;
-- получить все возраста и сколько раз они встречаются
select age, count(*)
from student
group by age;

-- получение студентов, у которых возраст больше 25 лет
select *
from student
where age > 25;
-- получение количества студентов, у которых возраст меньше 24 лет
select count(*)
from student
where age < 24;
-- получение студентов, которые работают или старше 30 лет
select *
from student
where age > 30
   or is_worker = true;
-- получение студентов, у которых в имени есть "слав"
select *
from student
where "like"(first_name, '%слав');
-- получение курсов, которые начинаются в 2022-м и заканчиваются после 2023
select *
from course
where (start between '2022-01-01' and '2022-12-31')
  and (finish > '2023-12-31');

-- получить название тех курсов, у которых есть уроки, начинающиеся в 10 часов
select title
from course
where id in (select distinct(course_id)
             from lesson
             where start_time = '10:00:00');
-- получить имена учеников, у которых уроки начнутся в 10:00
select first_name, last_name
from student
where id in (select distinct(student_id)
             from student_course
             where course_id in (select distinct (course_id)
                                 from lesson
                                 where start_time = '10:00:00'));

-- получаем уроки и их курсы (только те уроки, у которых есть курсы, и курсы, у которых есть уроки)
-- (inner) join
select *
from lesson l
         join course c on c.id = l.course_id
order by l.id;

-- получаем уроки и их курсы (получаем все уроки и курсы, у которых есть уроки)
-- left (outer) join
select *
from lesson l
         left join course c on c.id = l.course_id
order by l.id;

-- получаем уроки и их курсы (получаем только те уроки, у которых есть курсы, и все курсы)
-- right (outer) join
select *
from lesson l
         right join course c on c.id = l.course_id
order by l.id;

-- получаем уроки и их курсы (получаем все уроки и курсы)
-- full (outer) join
select *
from lesson l
         full join course c on c.id = l.course_id
order by l.id;