package ru.inno.services;

import lombok.RequiredArgsConstructor;
import ru.inno.models.User;
import ru.inno.repositories.UsersRepository;
import ru.inno.validators.EmailValidator;
import ru.inno.validators.PasswordValidator;

@RequiredArgsConstructor
public class UsersServiceImpl {

    private final UsersRepository usersRepository;
    private final EmailValidator emailValidator;
    private final PasswordValidator passwordValidator;

    public void signUp(String email, String password) {
        emailValidator.validate(email);
        passwordValidator.validate(password);

        User user = User.builder()
                .email(email)
                .password(password)
                .build();

        usersRepository.save(user);
    }
}
