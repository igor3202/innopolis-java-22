* Компиляция всех java-файлов в папку `target`

```
javac -cp lib/jcommander-1.82.jar -d target/ src/java/ru/inno/util/*/*.java
```

* Копирование файлов-ресурсов в папку `target`

```
cp -r src/resources/ target/
```

* Распаковать архив

```
jar xf jcommander-1.82.jar
```

* Переносим папку `com` в папку `target`

```
cp -r ../lib/com/ com
```

* Запуск программы внутри папки `target`

```
java ru/inno/util/app/Program
```

* Сборка JAR-архива с собственным `manifest`-файлом

```
jar -cvmf ../src/manifest.txt app.jar .
```

* Показать содержимое архива

```
jar tf app.jar 
```

* Запуск JAR-архива 

```
java -jar app.jar --files=file1,file2,file3
```
