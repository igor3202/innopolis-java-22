import java.util.List;
import java.util.stream.Stream;

public class Main3 {
    public static void main(String[] args) {
        Stream.of("Hello", "Bye", "how", "are", "You")
                .filter(word -> Character.isUpperCase(word.toCharArray()[0]))
                .map(String::toUpperCase) // a -> a.method => A::method
                .forEach(System.err::println); // a -> SomeClass.method(a) => SomeClass::method
    }
}
