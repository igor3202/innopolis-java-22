package ru.inno.validators;

public interface EmailValidator {

    /**
     * Проверяет корректность email
     * @param email почта, которую нужно проверить
     * @throws IllegalArgumentException если email не является корректным
     */
    void validate(String email) throws IllegalArgumentException;
}
