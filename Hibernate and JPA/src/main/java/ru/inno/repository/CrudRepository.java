package ru.inno.repository;

import java.util.List;

// CRUD - create, read, update, delete
public interface CrudRepository<T> {
    void save(T entity);

    List<T> findAll();

    T findById(Long id);
}
