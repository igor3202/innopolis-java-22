package ru.inno.repository;

import ru.inno.models.Student;

public interface StudentsRepository extends CrudRepository<Student> {
}
