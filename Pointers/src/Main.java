public class Main {
    public static void main(String[] args) {
        //  объявление объектной переменной marsel
        Human marsel;
        // создание объекта и присвоение его адреса переменной marsel
        marsel = new Human();
        // изменение состояния объекта
        marsel.height = 1.85;
        // объявление объектной переменной kirill
        Human kirill;
        // присваивание переменной адреса объекта, на который указывает marsel
        kirill = marsel;
        // изменение состояния исходного объекта
        kirill.height = 1.74;
        // вывод состояния объекта, к которому мы обратились через первоначальную переменную
        System.out.println(marsel.height); // 1.74, состояние изменено
    }
}
