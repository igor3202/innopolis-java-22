package ru.inno;

public interface Iterator<T> {
    /**
     * Возвращает элемент коллекции и сдвигает итератор дальше
     * @return новый элемент коллекции
     */
    T next();

    /**
     * Проверяет, есть ли элементы дальше от итератора
     * @return <code>true</code> - если есть, <code>false</code> - если нет
     */
    boolean hasNext();
}
