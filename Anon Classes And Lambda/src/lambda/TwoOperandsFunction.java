package lambda;

public interface TwoOperandsFunction {
    int apply(int x, int y);
}
