package lambda;

public interface NumbersProcessor {
    int process(int number);
}
